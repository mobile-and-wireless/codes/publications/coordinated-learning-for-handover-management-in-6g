function [predicted_D2F_gains,probabilities,net] = DNN_D2F_channel_prediction_W_Feedback(features,net,info,feed_AC,loss_feedback1,loss_feedback2,lambda)
gradients = dlfeval(@combinedLossFunction, info, feed_AC,loss_feedback1,loss_feedback2,lambda);
averageGrad = [];
averageSqGrad = [];
params = [];
for i = 1:numel(net.Layers)
    if isa(net.Layers(i), 'nnet.cnn.layer.FullyConnectedLayer') || isa(net.Layers(i), 'nnet.cnn.layer.Convolution2DLayer')
        params = [params; net.Layers(i).Weights(:); net.Layers(i).Bias(:)];
    end
end
updatedParams = adamupdate(params, sum(gradients),averageGrad,averageSqGrad,1000);
k = 1;
modify_able_NN = net.saveobj;
for i = 1:numel(modify_able_NN.Layers)
    if isa(modify_able_NN.Layers(i), 'nnet.cnn.layer.FullyConnectedLayer') || isa(modify_able_NN.Layers(i), 'nnet.cnn.layer.Convolution2DLayer')
        numWeights = numel(modify_able_NN.Layers(i).Weights);
        numBias = numel(modify_able_NN.Layers(i).Bias);
        modify_able_NN.Layers(i).Weights = reshape(updatedParams(k:k+numWeights-1), size(modify_able_NN.Layers(i).Weights));
        k = k + numWeights;
        modify_able_NN.Layers(i).Bias = reshape(updatedParams(k:k+numBias-1), size(modify_able_NN.Layers(i).Bias));
        k = k + numBias;
    end
end
net = net.loadobj(modify_able_NN);
predicted_D2F_gains = predict(net, features);
probabilities1 = (predicted_D2F_gains - min(predicted_D2F_gains)) / (max(predicted_D2F_gains) - min(predicted_D2F_gains));
capacity_levels = linspace(0, 1, 10);
probabilities12 = zeros(length(predicted_D2F_gains), 10);
for n = 1:length(predicted_D2F_gains)
    h_n = probabilities1(n);
    logits = exp(-(abs(capacity_levels - h_n)));
    max_logit = max(logits);
    logits = logits - max_logit;
    exp_logits = exp(logits);
    probabilities12(n, :) = exp_logits / sum(exp_logits);
end
probabilities12=probabilities12.';
probabilities=max(probabilities12);
end