function gradients = critic_gradients_WFB(network, input_data1, input_data2,target_Q_values,FB_loss,cap,lambda1) 
    lambda = lambda1;
    predictions = predict(network, input_data1, input_data2);
    target_Q_values = reshape(target_Q_values,length(predictions),[]);
    FB_loss(end+1:numel(cap))=0;
    externalLoss = -log(cap)+ max(0,FB_loss);
    loss = lambda*crossentropy(dlarray(real(mean(target_Q_values,2)),'BC'),predictions)+(1-lambda)*externalLoss;
    gradients = dlgradient(sum(loss), network.Learnables);
end