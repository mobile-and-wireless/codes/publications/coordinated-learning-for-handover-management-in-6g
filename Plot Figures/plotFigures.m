clc
clear
close all

Saving = 1;

%%FontSizes
FSLeg = 13; %legend font size
FSxL =16; %font size of x axis label
FSyL =16; %font size of y axis label
FS = 14;% font size for x and y numbers

%%LineWidths and Markers
LineWidthMarker = 1.5;
LineWidthLine = 0.25;
LineWidthLineThin = 0.01;
LineWidthLineThick = 0.4;
MarkerSizeSmallSmall = 7;
MarkerSizeSmall = 8;
MarkerSizeLarge = 10;

%% Convergence plots
ConverTextSize = 14;
ConverLineWidth = 2;

[convergcmin,convergUAV,HFRall,HFRUAVs,HFRUEs,HOCostall,HOCostGBS,HOCostUAV,lambda1,lambda2,NumHOForUAVall,NumHOForUAVUAVs,NumHOForUAVUEs,RSU2UAVs,RSU6UAVs,SatUserCreq,SumCapCminall,SumCapCminGBS,SumCapCminUAV,SumCapForUAVall,SumCapForUAVGBS,SumCapForUAVUAVs] = getplotsDATA();

figure;
plot(SumCapCminall{1,1}.XData,SumCapCminall{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(SumCapCminall{1,1}.XData,SumCapCminall{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(SumCapCminall{1,1}.XData,SumCapCminall{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(SumCapCminall{1,1}.XData,SumCapCminall{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)

set(gca,'FontSize', FS);

xlabel('c_{req} [Mbps]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
grid on
xticks([2 6 10 14 18 22]);
yticks([0 500 1000 1500 2000 2500]);
xlim([2 22]);

legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthEast')
if Saving == 1
    print(gcf, 'CminVsCapacityall.png', '-dpng','-r300');
end

figure;
plot(SumCapCminUAV{1,1}.XData,SumCapCminUAV{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(SumCapCminUAV{1,1}.XData,SumCapCminUAV{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(SumCapCminUAV{1,1}.XData,SumCapCminUAV{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(SumCapCminUAV{1,1}.XData,SumCapCminUAV{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)

set(gca,'FontSize', FS);

xlabel('c_{req} [Mbps]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
grid on
xticks([2 6 10 14 18 22]);
xlim([2 22]);
ylim([0 800]);
yticks([0 200 400 600 800]);
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthEast')
if Saving == 1
    print(gcf, 'CminVsCapacityUAVs.png', '-dpng','-r300');
end

figure;
plot(SumCapCminGBS{1,1}.XData,SumCapCminGBS{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(SumCapCminGBS{1,1}.XData,SumCapCminGBS{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(SumCapCminGBS{1,1}.XData,SumCapCminGBS{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(SumCapCminGBS{1,1}.XData,SumCapCminGBS{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)

set(gca,'FontSize', FS);

xlabel('c_{req} [Mbps]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
grid on
xticks([2 6 10 14 18 22]);
xlim([2 22]);
yticks([0 400 800 1200 1600]);
ylim([0 1600]);

legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthEast')
if Saving == 1
    print(gcf, 'CminVsCapacityGBS.png', '-dpng','-r300');
end

figure;
plot(RSU2UAVs{1,1}.XData,RSU2UAVs{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(RSU2UAVs{1,1}.XData,RSU2UAVs{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(RSU2UAVs{1,1}.XData,RSU2UAVs{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(RSU2UAVs{1,1}.XData,RSU2UAVs{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)

set(gca,'FontSize', FS);

xlabel('c_{req} [Mbps]','FontSize', FSxL);
ylabel('Ratio of Satisfied UEs [-]','FontSize', FSyL);
grid on
xticks([2 6 10 14 18 22]);
xlim([2 22]);
ylim([0.5 1]);

legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthWest')
if Saving == 1
    print(gcf, 'RS2UVsCreq.png', '-dpng','-r300');
end

figure;
plot(SatUserCreq{1,1}.XData,SatUserCreq{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(SatUserCreq{1,1}.XData,SatUserCreq{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(SatUserCreq{1,1}.XData,SatUserCreq{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(SatUserCreq{1,1}.XData,SatUserCreq{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)

set(gca,'FontSize', FS);

xlabel('c_{req} [Mbps]','FontSize', FSxL);
ylabel('Ratio of Satisfied UEs [-]','FontSize', FSyL);
grid on
xticks([2 6 10 14 18 22]);
xlim([2 22]);
ylim([0.5 1]);

legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthWest')
if Saving == 1
    print(gcf, 'RSUVsCreq.png', '-dpng','-r300');
end

figure;
plot(RSU6UAVs{1,1}.XData,RSU6UAVs{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(RSU6UAVs{1,1}.XData,RSU6UAVs{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(RSU6UAVs{1,1}.XData,RSU6UAVs{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(RSU6UAVs{1,1}.XData,RSU6UAVs{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)

set(gca,'FontSize', FS);

xlabel('c_{req} [Mbps]','FontSize', FSxL);
ylabel('Ratio of Satisfied UEs [-]','FontSize', FSyL);
grid on
xticks([2 6 10 14 18 22]);
xlim([2 22]);
ylim([0.5 1]);

legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthWest')
if Saving == 1
    print(gcf, 'RS6UVsCreq.png', '-dpng','-r300');
end

figure;
plot(100*HOCostall{1,1}.XData,HOCostall{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(100*HOCostall{1,1}.XData,HOCostall{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(100*HOCostall{1,1}.XData,HOCostall{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(100*HOCostall{1,1}.XData,HOCostall{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)

set(gca,'FontSize', FS);
xlim([100 1000])
ylim([1200 2200])
xlabel('Handover Cost, \mu [kb]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
grid on
xticks([100 200 300 400 500 600 700 800 900 1000]);
%xticks([200 400 600 800 1000]);
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','Northeast')
if Saving == 1
    print(gcf, 'HOcostVsCapall.png', '-dpng','-r300');
end

figure;
plot(100*HOCostGBS{1,1}.XData,HOCostGBS{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(100*HOCostGBS{1,1}.XData,HOCostGBS{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(100*HOCostGBS{1,1}.XData,HOCostGBS{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(100*HOCostGBS{1,1}.XData,HOCostGBS{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)
xlim([100 1000])
ylim([900 1400])
set(gca,'FontSize', FS);

xlabel('Handover Cost, \mu [kb]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
grid on
%xticks([200 400 600 800 1000]);
xticks([100 200 300 400 500 600 700 800 900 1000]);
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','Northeast')
if Saving == 1
    print(gcf, 'HOcostVsCapGBS.png', '-dpng','-r300');
end

figure;
plot(100*HOCostUAV{1,1}.XData,HOCostUAV{1,4}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
hold on
plot(100*HOCostUAV{1,1}.XData,HOCostUAV{1,3}.YData,'b--x','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthLine)
plot(100*HOCostUAV{1,1}.XData,HOCostUAV{1,2}.YData,'-.+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(100*HOCostUAV{1,1}.XData,HOCostUAV{1,1}.YData,'k:v','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthLine)
xlim([100 1000])
ylim([450 750])
set(gca,'FontSize', FS);

xlabel('Handover Cost, \mu [kb]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
%xticks([200 400 600 800 1000]);
xticks([100 200 300 400 500 600 700 800 900 1000]);
grid on

legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','Northeast')
if Saving == 1
    print(gcf, 'HOcostVsCapUAV.png', '-dpng','-r300');
end

figure;
%b = bar(SumCapForUAVall{1,1}.XData,[SumCapForUAVall{1,4}.YData;SumCapForUAVall{1,3}.YData;SumCapForUAVall{1,2}.YData;SumCapForUAVall{1,1}.YData]);
%b(1, 1).FaceColor = 'r';
%b(1, 2).FaceColor = 'b';
%b(1, 3).FaceColor = '[0.39,0.83,0.07]';
%b(1, 4).FaceColor = 'k';

% Marker + Line only
plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

% plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,4}.YData,'r:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
% plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,3}.YData,'b:','LineWidth',LineWidthLine)
% plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,2}.YData,':','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
% plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)

% plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,4}.YData,'r--','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
% plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,3}.YData,'b--','LineWidth',LineWidthLine)
% plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,2}.YData,'--','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
% plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,1}.YData,'k--','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)

plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(SumCapForUAVall{1,1}.XData,SumCapForUAVall{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)
ylim([600 2200]);
set(gca,'FontSize', FS);

xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
grid on
%
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthEast')
if Saving == 1
print(gcf, 'SumCapall_MarkerLine.png', '-dpng','-r300');
end

figure;
plot(SumCapForUAVGBS{1,1}.XData,SumCapForUAVGBS{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(SumCapForUAVGBS{1,1}.XData,SumCapForUAVGBS{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(SumCapForUAVGBS{1,1}.XData,SumCapForUAVGBS{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(SumCapForUAVGBS{1,1}.XData,SumCapForUAVGBS{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

plot(SumCapForUAVGBS{1,1}.XData,SumCapForUAVGBS{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(SumCapForUAVGBS{1,1}.XData,SumCapForUAVGBS{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(SumCapForUAVGBS{1,1}.XData,SumCapForUAVGBS{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(SumCapForUAVGBS{1,1}.XData,SumCapForUAVGBS{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)

set(gca,'FontSize', FS);
xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
grid on
ylim([400 1400]);
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthEast')
if Saving == 1
print(gcf, 'SumCapGBS_MarkerLine.png', '-dpng','-r300');
end

figure;
plot(SumCapForUAVUAVs{1,1}.XData,SumCapForUAVUAVs{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(SumCapForUAVUAVs{1,1}.XData,SumCapForUAVUAVs{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(SumCapForUAVUAVs{1,1}.XData,SumCapForUAVUAVs{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(SumCapForUAVUAVs{1,1}.XData,SumCapForUAVUAVs{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

plot(SumCapForUAVUAVs{1,1}.XData,SumCapForUAVUAVs{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(SumCapForUAVUAVs{1,1}.XData,SumCapForUAVUAVs{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(SumCapForUAVUAVs{1,1}.XData,SumCapForUAVUAVs{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(SumCapForUAVUAVs{1,1}.XData,SumCapForUAVUAVs{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)

set(gca,'FontSize', FS);
xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('Sum Capacity [Mbps]','FontSize', FSyL);
grid on

legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthEast')
if Saving == 1
print(gcf, 'SumCapUAVs_MarkerLine.png', '-dpng','-r300');
end

figure;
plot(NumHOForUAVall{1,1}.XData,NumHOForUAVall{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(NumHOForUAVall{1,1}.XData,NumHOForUAVall{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(NumHOForUAVall{1,1}.XData,NumHOForUAVall{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(NumHOForUAVall{1,1}.XData,NumHOForUAVall{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

plot(NumHOForUAVall{1,1}.XData,NumHOForUAVall{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(NumHOForUAVall{1,1}.XData,NumHOForUAVall{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(NumHOForUAVall{1,1}.XData,NumHOForUAVall{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(NumHOForUAVall{1,1}.XData,NumHOForUAVall{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)

set(gca,'FontSize', FS);
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','NorthWest')
xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('Number of Handovers [-]','FontSize', FSyL);
grid on
if Saving == 1
print(gcf, 'HOall_MarkerLine.png', '-dpng','-r300');
end

figure;
plot(NumHOForUAVUAVs{1,1}.XData,NumHOForUAVUAVs{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(NumHOForUAVUAVs{1,1}.XData,NumHOForUAVUAVs{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(NumHOForUAVUAVs{1,1}.XData,NumHOForUAVUAVs{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(NumHOForUAVUAVs{1,1}.XData,NumHOForUAVUAVs{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

plot(NumHOForUAVUAVs{1,1}.XData,NumHOForUAVUAVs{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(NumHOForUAVUAVs{1,1}.XData,NumHOForUAVUAVs{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(NumHOForUAVUAVs{1,1}.XData,NumHOForUAVUAVs{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(NumHOForUAVUAVs{1,1}.XData,NumHOForUAVUAVs{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)

set(gca,'FontSize', FS);

xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('Number of Handovers [-]','FontSize', FSyL);
grid on

legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','NorthWest')
if Saving == 1
print(gcf, 'HOUAVs_MarkerLine.png', '-dpng','-r300');
end

figure;
plot(NumHOForUAVUEs{1,1}.XData,NumHOForUAVUEs{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(NumHOForUAVUEs{1,1}.XData,NumHOForUAVUEs{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(NumHOForUAVUEs{1,1}.XData,NumHOForUAVUEs{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(NumHOForUAVUEs{1,1}.XData,NumHOForUAVUEs{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

plot(NumHOForUAVUEs{1,1}.XData,NumHOForUAVUEs{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(NumHOForUAVUEs{1,1}.XData,NumHOForUAVUEs{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(NumHOForUAVUEs{1,1}.XData,NumHOForUAVUEs{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(NumHOForUAVUEs{1,1}.XData,NumHOForUAVUEs{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)

set(gca,'FontSize', FS);

xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('Number of Handovers [-]','FontSize', FSyL);
grid on
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','NorthWest')
if Saving == 1
print(gcf, 'HOUEs_MarkerLine.png', '-dpng','-r300');
end

figure;
plot(HFRall{1,1}.XData,HFRall{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(HFRall{1,1}.XData,HFRall{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(HFRall{1,1}.XData,HFRall{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(HFRall{1,1}.XData,HFRall{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

plot(HFRall{1,1}.XData,HFRall{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(HFRall{1,1}.XData,HFRall{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(HFRall{1,1}.XData,HFRall{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(HFRall{1,1}.XData,HFRall{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)

set(gca,'FontSize', FS);
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','Northeast')
xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('HFR [%]','FontSize', FSyL);
grid on
ylim([0.25 0.55])
if Saving == 1
print(gcf, 'HFRall_MarkerLine.png', '-dpng','-r300');
end

figure;
plot(HFRUEs{1,1}.XData,HFRUEs{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(HFRUEs{1,1}.XData,HFRUEs{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(HFRUEs{1,1}.XData,HFRUEs{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(HFRUEs{1,1}.XData,HFRUEs{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

plot(HFRUEs{1,1}.XData,HFRUEs{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(HFRUEs{1,1}.XData,HFRUEs{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(HFRUEs{1,1}.XData,HFRUEs{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(HFRUEs{1,1}.XData,HFRUEs{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)

set(gca,'FontSize', FS);
xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('HFR [%]','FontSize', FSyL);
grid on
ylim([0.2 0.55]);
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','SouthWest')

if Saving == 1
print(gcf, 'HFRUEs_MarkerLine.png', '-dpng','-r300');
end

figure;
plot(HFRUAVs{1,1}.XData,HFRUAVs{1,4}.YData,'ro','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthMarker)
hold on
plot(HFRUAVs{1,1}.XData,HFRUAVs{1,3}.YData,'bx','MarkerSize', MarkerSizeLarge,'LineWidth',LineWidthMarker)
plot(HFRUAVs{1,1}.XData,HFRUAVs{1,2}.YData,'+','MarkerSize', MarkerSizeLarge,'Color',[0.39,0.83,0.07],'LineWidth',LineWidthMarker)
plot(HFRUAVs{1,1}.XData,HFRUAVs{1,1}.YData,'kv','MarkerSize', MarkerSizeSmallSmall,'LineWidth',LineWidthMarker)

plot(HFRUAVs{1,1}.XData,HFRUAVs{1,4}.YData,'r','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThin)
plot(HFRUAVs{1,1}.XData,HFRUAVs{1,3}.YData,'b--','LineWidth',LineWidthLine)
plot(HFRUAVs{1,1}.XData,HFRUAVs{1,2}.YData,'-.','Color',[0.39,0.83,0.07],'LineWidth',LineWidthLine)
plot(HFRUAVs{1,1}.XData,HFRUAVs{1,1}.YData,'k:','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLineThick)

set(gca,'FontSize', FS);
xlabel('Number of UAVs, F [-]','FontSize', FSxL);
ylabel('HFR [%]','FontSize', FSyL);
grid on
legend('Proposed DDPG CIO','w/o. coordination','AC-DRL CIO [15]','Adap. CIO [6]','FontSize', FSLeg,'Location','Northeast')
ylim([0.2 0.6]);
if Saving == 1
print(gcf, 'HFRUAVs_MarkerLine.png', '-dpng','-r300');
end

figure
plot(convergcmin{1,1}.XData,convergcmin{1,4}.YData,'r','LineWidth',LineWidthLine)
hold on
plot(convergcmin{1,1}.XData,convergcmin{1,3}.YData,'b','LineWidth',LineWidthLine)
plot(convergcmin{1,1}.XData,convergcmin{1,2}.YData,'r:','LineWidth',LineWidthLine)
plot(convergcmin{1,1}.XData,convergcmin{1,1}.YData,'b:','LineWidth',LineWidthLine)
grid on

line([61,61],[2250,2540],'Color','r','LineWidth',ConverLineWidth)
text(63,2490,'Convergence','Color','red','FontSize',ConverTextSize)
line([89,89],[2020,2300],'Color','b','LineWidth',ConverLineWidth)
text(92,2090,'Convergence','Color','blue','FontSize',ConverTextSize)
line([68,68],[1710,1990],'Color','r','LineWidth',ConverLineWidth)
text(71,1920,'Convergence','Color','red','FontSize',ConverTextSize)
line([95,95],[1450,1710],'Color','b','LineWidth',ConverLineWidth)
text(98,1510,'Convergence','Color','blue','FontSize',ConverTextSize)

dummyh = line(nan, nan, 'Linestyle', '-', 'Marker', 'none', 'Color', 'r' );
dummyh = line(nan, nan, 'Linestyle', ':', 'Marker', 'none', 'Color', 'r', 'LineWidth',LineWidthLine);
legend('Proposed DDPG CIO','w/o. coordination','','','','','','','C_{req} = 14 Mbps','C_{req} = 22 Mbps','Location','southeast')
yticks([800 1000 1200 1400 1600 1800 2000 2200 2400 2600]);
ylim([800 2600])
set(gca,'FontSize',14)
xlabel('Number of Handovers [-]')
ylabel('Sum Capacity [Mbps]')
if Saving == 1
    print(gcf, 'DDPGConvCreq.png', '-dpng','-r300');
end

figure
plot(convergUAV{1,1}.XData,convergUAV{1,4}.YData,'r','LineWidth',LineWidthLine)
hold on
plot(convergUAV{1,1}.XData,convergUAV{1,3}.YData,'b','LineWidth',LineWidthLine)
plot(convergUAV{1,1}.XData,convergUAV{1,2}.YData,'r:','LineWidth',LineWidthLine)
plot(convergUAV{1,1}.XData,convergUAV{1,1}.YData,'b:','LineWidth',LineWidthLine)
grid on

line([55,55],[1860,2140],'Color','r','LineWidth',2)
text(58,2110,'Convergence','Color','red','FontSize',14)
line([97,97],[1920,1650],'Color','b','LineWidth',2)
text(100,1890,'Convergence','Color','blue','FontSize',14)
line([62,62],[1430,1640],'Color','r','LineWidth',2)
text(65,1480,'Convergence','Color','red','FontSize',14)
line([99,99],[1400,1160],'Color','b','LineWidth',2)
text(102,1220,'Convergence','Color','blue','FontSize',14)

dummyh = line(nan, nan, 'Linestyle', '-', 'Marker', 'none', 'Color', 'r');
dummyh = line(nan, nan, 'Linestyle', ':', 'Marker', 'none', 'Color', 'r', 'LineWidth',LineWidthLine);
legend('Proposed DDPG CIO','w/o. coordination','','','','','','','F = 1','F = 6','Location','southeast')
yticks([400 600 800 1000 1200 1400 1600 1800 2000 2200]);
ylim([400 2200])
set(gca,'FontSize',14)
xlabel('Number of Handovers [-]')
ylabel('Sum Capacity [Mbps]')

if Saving == 1
    print(gcf, 'DDPGConvUAVs.png', '-dpng','-r300');
end

figure
%colororder({'r','m'})
yyaxis left
plot(lambda1{1,1}.XData,lambda1{1,2}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
xlabel('Scaling Parameter \lambda_1 [-]')
ylabel('Sum Capacity [Mbps]')
ylim([1800 1860])
yyaxis right
plot(lambda1{1,1}.XData,lambda1{1,1}.YData,'--d','Color',[1.00,0.41,0.16],'MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
ylabel('Number of Handovers [-]')
ylim([40 65])
grid on
legend('Sum capacity','Number of handovers','Location','northeast')

xlim([0 1])
ax=gca;
ax.YAxis(2).Color = [1.00,0.41,0.16];
ax.YAxis(1).Color = 'r';
set(gca,'FontSize',14)
if Saving == 1
    print(gcf, 'SumCapConvrateVslambda1.png', '-dpng','-r300');
end

figure
%colororder({'r','m'})
yyaxis left
plot(lambda2{1,1}.XData,lambda2{1,2}.YData,'r-o','MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
xlabel('Scaling Parameter \lambda_2 [-]')
ylabel('Sum Capacity [Mbps]')
ylim([1810 1860])

yyaxis right
plot(lambda2{1,1}.XData,lambda2{1,1}.YData,'--d','Color',[1.00,0.41,0.16],'MarkerSize', MarkerSizeSmall,'LineWidth',LineWidthLine)
ylabel('Number of Handovers [-]')
ylim([40 65])
grid on
legend('Sum capacity','Number of handovers','Location','northeast')

xlim([0 1])
ax=gca;
ax.YAxis(2).Color = [1.00,0.41,0.16];
ax.YAxis(1).Color = 'r';
set(gca,'FontSize',14)
if Saving == 1
    print(gcf, 'SumCapConvrateVslambda2.png', '-dpng','-r300');
end