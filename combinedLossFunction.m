function combinedLoss = combinedLossFunction(info,feed_AC,loss_feedback1,loss_feedback2,lambda)
    %manualLossWeight =  0.5;
    manualLossWeight =  lambda;
    manualLoss1 = -log(feed_AC)+max(0,loss_feedback1)+max(0,loss_feedback2);
    combinedLoss =  manualLossWeight* info.TrainingLoss + (1 - manualLossWeight)*sum(manualLoss1);
end