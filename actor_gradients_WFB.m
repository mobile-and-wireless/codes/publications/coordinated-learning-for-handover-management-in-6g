function gradients = actor_gradients_WFB(network, input_data,reward,FB_loss,cap,lambda1)   
    lambda = lambda1;
    predictions = predict(network, input_data);
    target_Q_values = reshape(reward,[],size(predictions,1));
    FB_loss(end+1:numel(cap))=0;
    ext_loss = -log(cap) + max(0,FB_loss);
    loss = lambda*crossentropy(dlarray(target_Q_values,'BC'),predictions)+(1-lambda)*sum(ext_loss);
    gradients = dlgradient(sum(loss), network.Learnables);
end