function actor = create_actor_network(k)
actorNetwork = [featureInputLayer(k) 
    fullyConnectedLayer(32)
    reluLayer
    fullyConnectedLayer(16)
    reluLayer
    fullyConnectedLayer(8)
    fullyConnectedLayer(k)];
actor = dlnetwork(actorNetwork);
end