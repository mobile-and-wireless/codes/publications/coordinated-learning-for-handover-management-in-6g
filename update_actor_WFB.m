function actor = update_actor_WFB(actor, ~, batch, actor_lr, ~, FB_loss,cap,lambda1)
    states = cat(1, batch.state);
    action = cat(1, batch.action);
    %reward = cat(1, batch.reward);
    %next_states = cat(1, batch.next_state); 
    actor_gradients = gradient_Actor_WFB(actor, dlarray(states,'BC'),action,FB_loss,cap,lambda1);
    averageGrad = [];
    averageSqGrad = [];    
    actor = adamupdate(actor, actor_gradients,averageGrad,averageSqGrad,1000,actor_lr);
end