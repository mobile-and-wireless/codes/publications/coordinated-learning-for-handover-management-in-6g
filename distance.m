function d = distance(Number_MS,Number_BS,PosMS,PosBS,SimStep,UE_pos)
d=zeros(Number_MS,Number_BS);
for i=1:Number_MS
    for j=1:Number_BS
        d(i,j)=sqrt((abs(PosBS(j,1)-PosMS(SimStep,2*i-1)))^2 + (abs(PosBS(j,2)-PosMS(SimStep,2*i)))^2+(abs(PosBS(j,3)-UE_pos(i,3)))^2);
    end
end
end