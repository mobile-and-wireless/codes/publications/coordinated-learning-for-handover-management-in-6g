function [predicted_D2F_gains,probabilities,net,info] = DNN_D2F_channel_prediction(features,targets)
MaxEpochs = 1000;
layers = [sequenceInputLayer(size(features,1))
          fullyConnectedLayer(40)
          sigmoidLayer
          fullyConnectedLayer(30)
          sigmoidLayer
          fullyConnectedLayer(20)
          sigmoidLayer
          fullyConnectedLayer(size(targets,1))
          regressionLayer
];
options = trainingOptions('adam', ...
    'MaxEpochs', MaxEpochs, ...
    'MiniBatchSize', 64, ...
    'InitialLearnRate', 0.01);
options.Verbose = false;
[net, info] = trainNetwork(features, targets, layers, options);
predicted_D2F_gains = predict(net, features);
probabilities1 = (predicted_D2F_gains - min(predicted_D2F_gains)) / (max(predicted_D2F_gains) - min(predicted_D2F_gains));
capacity_levels = linspace(0, 1, 10);
probabilities12 = zeros(length(predicted_D2F_gains), 10);
for n = 1:length(predicted_D2F_gains)
    h_n = probabilities1(n);
    logits = exp(-(abs(capacity_levels - h_n)));
    max_logit = max(logits);
    logits = logits - max_logit;
    exp_logits = exp(logits);
    probabilities12(n, :) = exp_logits / sum(exp_logits);
end
probabilities12=probabilities12.';
probabilities=max(probabilities12);
end