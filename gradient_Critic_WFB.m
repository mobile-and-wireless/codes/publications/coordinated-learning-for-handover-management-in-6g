function gradients = gradient_Critic_WFB(network, input_data1,input_data2,target_Q_values,FB_loss,cap,lambda1)
    gradients = dlfeval(@critic_gradients_WFB, network, input_data1, input_data2,target_Q_values,FB_loss,cap,lambda1);
end