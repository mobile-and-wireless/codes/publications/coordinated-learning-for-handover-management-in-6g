function [action,actornet,criticnet,replay_buffer,state] = CIO_Decision_WFB(~,load,Bn,SINR_up,SINR_UAV,FB_loss,cap,actornet,criticnet,replay_buffer,state,cost,lambda1)
gamma = 0.99;
actor_lr = 0.001;
critic_lr = 0.01;
CIO_min = -6;
CIO_max = 6;
snir=[SINR_UAV;SINR_up];
state = load;
action = select_action(actornet, state, CIO_min, CIO_max);
[next_state, reward] = perform_action_prop(state, action,Bn,snir,CIO_min,CIO_max,cost);
experience = struct('state', state, 'action', action, 'reward', reward, 'next_state', next_state);
replay_buffer = [replay_buffer, experience];
criticnet = update_critic_WFB(actornet, criticnet, replay_buffer, critic_lr, gamma,FB_loss,cap,lambda1);
actornet = update_actor_WFB(actornet, criticnet, replay_buffer, actor_lr, gamma,FB_loss,cap,lambda1);
state = next_state;
end