function PL = pathloss_UAV(Number_MS,Number_BS,d,f,~)
PL=zeros(Number_MS,Number_BS);
for i=1:Number_MS
    for j=1:Number_BS
       PL(i,j) = 28.0 + 22*log10(d(i,j)) + 20*log10(f); %UMa_AV
       %RMa_AV
       %PL(i,j) = max(23.9-(1.8*log10(h_UAV)),20)*log10(d(i,j))+20*log10((40*pi*f)/2)
    end
end
end