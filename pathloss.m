function PL = pathloss(Number_MS,Number_BS,d,f)
PL=zeros(Number_MS,Number_BS);
for i=1:Number_MS
    for j=1:Number_BS
       PL(i,j) = 20*log10(d(i,j))+20*log10(f*10e9)-147.55;
    end
end
end

