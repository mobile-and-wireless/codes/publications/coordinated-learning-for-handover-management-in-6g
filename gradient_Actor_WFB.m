function gradients = gradient_Actor_WFB(network, input_data,reward,FB_loss,cap,lambda1)
    gradients = dlfeval(@actor_gradients_WFB, network, input_data,reward,FB_loss,cap,lambda1);
end